`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 29.02.2024 12:05:19
// Design Name:
// Module Name: zad1
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module zad(
    input [15:0] sw_i,
    output reg [7:0] an_o,
    output reg [7:0] seg_o
    );

    always @*
    begin
        an_o = 8'b11111110;
       if (^sw_i)
         seg_o = 8'b00000011;
       else
         seg_o = 8'b01100001;
     end

endmodule
