`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.02.2024 15:39:34
// Design Name: 
// Module Name: tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb;
 reg clk_i;
    reg rst_i;
    wire led_o;

    zad uut (
        .clk_i(clk_i),
        .rst_i(rst_i),
        .led_o(led_o)
    );

    always begin
        #5 clk_i = ~clk_i;
    end

    initial begin
        clk_i = 0;
        rst_i = 0;

        #70 rst_i = 1;
        #10 rst_i = 0;

        #300 $finish;
    end
endmodule
