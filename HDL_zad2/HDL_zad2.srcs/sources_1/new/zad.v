`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.02.2024 15:38:33
// Design Name: 
// Module Name: zad
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module zad(
    input clk_i,
    input rst_i,
    output reg led_o
);
    parameter N = 100000000;

    reg [31:0] counter;

    always @(posedge clk_i or posedge rst_i)
        if (rst_i)
          begin
             counter = 0;
             led_o = 1'b0;
          end
        else
          begin
            if (counter < N)
                counter = counter + 1;
            else
                counter = 1;

            if (counter > (N / 2))
                led_o = 1'b1;
            else
                led_o = 1'b0;
          end

endmodule
