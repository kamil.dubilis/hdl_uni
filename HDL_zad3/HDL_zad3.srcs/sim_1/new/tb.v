`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.02.2024 15:39:34
// Design Name: 
// Module Name: tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb;
   reg clk_i;
    reg rst_i;
    reg RXD_i;;
    wire TXD_o;

    zad uut (
        .clk_i(clk_i),
        .rst_i(rst_i),
        .RXD_i(RXD_i),
        .TXD_o(TXD_o)
    );

    always begin
        #5 clk_i = ~clk_i;
    end

    initial begin
        clk_i = 0;
        rst_i = 1;
        RXD_i = 1;

       #1000000;
       #104160 RXD_i = 0;
       #104160 RXD_i = 1;
       #104160 RXD_i = 1;
       #104160 RXD_i = 0;
       #104160 RXD_i = 0;
       #104160 RXD_i = 1;
       #104160 RXD_i = 0;
       #104160 RXD_i = 1;
       #104160 RXD_i = 0;
       #104160 RXD_i = 1;

       #1000000 $finish;
    end
endmodule
