`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.02.2024 18:14:33
// Design Name: 
// Module Name: clock
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module clock(
    input clk_i,
    output reg clk_o
    );

    parameter N = 10417;

    reg [31:0] counter;

    always @(posedge clk_i)
        begin
            if (counter < N)
                counter = counter + 1;
            else
                counter = 1;

            if (counter > (N / 2))
                clk_o = 1'b1;
            else
                clk_o = 1'b0;
        end

endmodule
