`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 29.02.2024 15:38:33
// Design Name:
// Module Name: zad
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module zad(
        input      clk_i,
        input      rst_i,
        input      RXD_i,
        output reg TXD_o
    );

   reg [2:0] bitN;
   reg       trans_i;
   reg       trans_o;
   wire      trans_clk;
   reg [0:7] data;

   clock clk9600(clk_i, trans_clk);

   always @(posedge trans_clk)
     begin
        if (trans_i)
          begin
             data[bitN] = RXD_i;
             bitN = bitN + 1;

             if (bitN == 0)
               begin
                  trans_i = 1'b0;
                  data = data + 8'b00000100;
                  TXD_o = 1'b0;
                  trans_o = 1'b1;
               end
          end
        else if (~RXD_i)
          begin
             trans_i = 1'b1;
             bitN = 0;
          end

        else if (trans_o)
          begin
             TXD_o = data[bitN];
             bitN = bitN + 1;

             if (bitN == 0)
                trans_o = 1'b0;
          end
        else
          TXD_o = 1'b1;
     end
endmodule
