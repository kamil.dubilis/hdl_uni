`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.02.2024 15:39:34
// Design Name: 
// Module Name: tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb;
        reg clk_i;
        reg rst_i;
        reg button_hr_i;
        reg button_min_i;
        reg button_test_i;
        wire [7:0] led7_an_o;
        wire [7:0] led7_seg_o;

    zad uut (
        .clk_i(clk_i),
        .rst_i(rst_i),
        .button_hr_i(button_hr_i),
        .button_min_i(button_min_i),
        .button_test_i(button_test_i),
        .led7_an_o(led7_an_o),
        .led7_seg_o(led7_seg_o)
);

    always begin
        #5 clk_i = ~clk_i;
    end

    initial begin
       clk_i = 0;
       button_test_i = 0;
       button_hr_i = 0;

       #500 button_test_i = 1'b1;

       #250000000 button_hr_i = 1'b1;
       #80000000 button_hr_i = 1'b0;
    end
endmodule
