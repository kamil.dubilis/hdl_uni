`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.02.2024 21:01:43
// Design Name: 
// Module Name: debounce
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module debounce(
    input wire clk_i,
    input wire  btn_i,
    output reg btn_o
    );

   parameter   debounce_time = 5000000;

    reg [23:0] debounce_counter;
    reg btn_last;

    always @(posedge clk_i) begin
        if (btn_i == btn_last)
            debounce_counter = debounce_counter + 1;
        else
            debounce_counter = 0;

        if (debounce_counter >= debounce_time)
            btn_o = btn_i;

        btn_last = btn_i;
    end
endmodule
