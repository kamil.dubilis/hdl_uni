`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.02.2024 21:27:12
// Design Name: 
// Module Name: minute
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module minute(
    input wire [0:18] seconds,
    output reg [0:3] digit_1,
    output reg [0:3] digit_2
    );

   always @(seconds)
     begin
        digit_1 = (seconds / 600) % 6;
        digit_2 = (seconds / 60) % 10;
     end
endmodule
