`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.02.2024 21:52:44
// Design Name: 
// Module Name: timesec
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module timesec(
    input wire clk_i,
    input wire btn_hour_i,
    input wire btn_min_i,
    input wire btn_rst_i,
    output reg [0:18] seconds
    );

   always @(posedge clk_i or posedge btn_hour_i or posedge btn_min_i or posedge btn_rst_i)
      if (btn_rst_i)
        seconds = 0;
      else if (clk_i)
        if (seconds < 86400)
          seconds = seconds + 1;
        else
          seconds = 0;
      else if (btn_hour_i)
        if (seconds < 82800)
          seconds = seconds + 3600;
        else
          seconds = seconds - 82800;
      else if (btn_min_i)
        if (seconds < 86340)
          seconds = seconds + 60;
        else
          seconds = seconds - 86340;
endmodule
