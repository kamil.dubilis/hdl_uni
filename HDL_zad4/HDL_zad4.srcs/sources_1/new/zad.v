`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.02.2024 15:38:33
// Design Name: 
// Module Name: zad
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module zad(
        input clk_i,
        input rst_i,
        input button_hr_i,
        input button_min_i,
        input button_test_i,
        output reg [7:0] led7_an_o,
        output reg [7:0] led7_seg_o
           );
   wire       clk_1s;
   wire       clk_1ms;
   wire       button_hr_d;
   wire       button_min_d;
   wire       button_test_d;

   wire [0:18] seconds;

   wire [0:3]  hour_digit_1;
   wire [0:3]  hour_digit_2;
   wire [0:3]  min_digit_1;
   wire [0:3]  min_digit_2;

   wire [0:27] seg_digits;

   reg [0:1]   seg_num;

   clock clk1s(clk_i, clk_1s,button_test_d);
   clock #(100000) clk1ms(clk_i, clk_1ms,1'b0);
   debounce btnhrd(clk_i, button_hr_i, button_hr_d);
   debounce btnmind(clk_i, button_min_i, button_min_d);
   debounce btntestd(clk_i, button_test_i, button_test_d);
   timesec secs(clk_1s, button_hr_d, button_min_d, rst_i, seconds);
   hour hours(seconds, hour_digit_1, hour_digit_2);
   minute minutes(seconds, min_digit_1, min_digit_2);
   digit hd1(hour_digit_1, seg_digits[0:6]);
   digit hd2(hour_digit_2, seg_digits[7:13]);
   digit md1(min_digit_1, seg_digits[14:20]);
   digit md2(min_digit_2, seg_digits[21:27]);

   always @(posedge clk_1ms)
        if (seg_num < 4)
          begin
             seg_num = seg_num + 1;
             led7_an_o = ~(8'b1 << (4 - seg_num));
             if (seg_num == 1)
               led7_seg_o = {seg_digits[7*seg_num+:7],clk_1s};
             else
               led7_seg_o = {seg_digits[7*seg_num+:7],1'b1};
          end
        else
          seg_num = 0;
endmodule
