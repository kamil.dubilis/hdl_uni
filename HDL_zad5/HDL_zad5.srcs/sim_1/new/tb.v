`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 29.02.2024 14:52:51
// Design Name:
// Module Name: zad1_tb
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module tb(

    );
    reg [15:0] sw_i;
        wire [7:0] an_o, seg_o;

        zad dut (
            .sw_i(sw_i),
            .an_o(an_o),
            .seg_o(seg_o)
        );

        initial begin
            sw_i = 16'b0000000000000000;

            #10 sw_i = 16'b0000000000000001;
            #10 sw_i = 16'b0000000000001001;
            #10 sw_i = 16'b0000000100001001;
            #10 sw_i = 16'b0000000100101001;
            #10 sw_i = 16'b0000000000101001;

            #10 $finish;
        end
endmodule
