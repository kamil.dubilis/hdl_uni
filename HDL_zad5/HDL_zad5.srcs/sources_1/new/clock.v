`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 29.02.2024 21:12:59
// Design Name:
// Module Name: clock
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module clock #(parameter N = 100000000) (
    input clk_i,
    output reg clk_o,
    input btn_i
    );

    reg [31:0] counter;

    always @(posedge clk_i)
        begin
            if (counter < (N/(999*btn_i + 1)))
                counter = counter + 1;
            else
                counter = 1;

            if (counter > ((N/(999*btn_i + 1)) / 2))
                clk_o = 1'b1;
            else
                clk_o = 1'b0;
        end
endmodule
