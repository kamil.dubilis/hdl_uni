`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.03.2024 13:41:26
// Design Name: 
// Module Name: digit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module digit(
    input wire [0:7] char,
    output reg [0:3] input_digit
    );

   always @(char)
     if (char == 8'h70)
       input_digit = 0;
     else if (char == 8'h69)
       input_digit = 1;
     else if (char == 8'h72)
       input_digit = 2;
     else if (char == 8'h7a)
       input_digit = 3;
     else if (char == 8'h6b)
       input_digit = 4;
     else if (char == 8'h73)
       input_digit = 5;
     else if (char == 8'h74)
       input_digit = 6;
     else if (char == 8'h6c)
       input_digit = 7;
     else if (char == 8'h75)
       input_digit = 8;
     else if (char == 8'h7d)
       input_digit = 9;
endmodule
