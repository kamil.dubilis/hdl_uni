`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 29.02.2024 23:03:42
// Design Name:
// Module Name: digit
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module disp_seg(
        input wire [0:13] num,
        output reg [0:27]  seg_digits
    );

   always @(num)
     begin
        seg_digits[0:6] = get_seg_digit(num / 1000);
        seg_digits[7:13] = get_seg_digit((num / 100) % 10);
        seg_digits[14:20] = get_seg_digit((num / 10) % 10);
        seg_digits[21:27] = get_seg_digit(num % 10);
     end

  function [0:6] get_seg_digit;
    input [0:3] digit;
    case (digit)
      0: get_seg_digit = 7'b0000001;
      1: get_seg_digit = 7'b1001111;
      2: get_seg_digit = 7'b0010010;
      3: get_seg_digit = 7'b0000110;
      4: get_seg_digit = 7'b1001100;
      5: get_seg_digit = 7'b0100100;
      6: get_seg_digit = 7'b0100000;
      7: get_seg_digit = 7'b0001111;
      8: get_seg_digit = 7'b0000000;
      9: get_seg_digit = 7'b0000100;
      default: get_seg_digit = 7'b1111111;
    endcase
  endfunction

endmodule
