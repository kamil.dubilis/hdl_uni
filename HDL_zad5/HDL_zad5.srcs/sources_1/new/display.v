`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.03.2024 13:41:26
// Design Name: 
// Module Name: display
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module display(
    input wire clk_i,
    input wire rst_i,
    input wire ps2_clk_i,
    input wire ps2_data_i,
    output reg [7:0] led7_an_o,
    output reg [7:0] led7_seg_o
    );

   wire       clk_1ms;
   wire [0:13] num;
   wire [0:27] seg_digits;
   reg [0:1]   seg_num;

   math calc(clk_i, rst_i, ps2_clk_i, ps2_data_i, num);
   clock #(100000) clk1ms(clk_i, clk_1ms, 1'b0);
   disp_seg seg(num, seg_digits);

   always @(posedge clk_1ms)
        if (seg_num < 4)
          begin
             seg_num = seg_num + 1;
             led7_an_o = 8'b1 << (4 - seg_num);
             led7_seg_o = {seg_digits[7*seg_num+:7],1'b1};
          end
        else
          seg_num = 0;
endmodule
