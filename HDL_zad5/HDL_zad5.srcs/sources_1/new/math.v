`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.03.2024 13:48:38
// Design Name: 
// Module Name: math
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module math(
    input wire clk_i,
    input wire rst_i,
    input wire ps2_clk_i,
    input wire ps2_data_i,
    output reg [0:13] num
    );

   wire [0:2]    oper;
   reg          add_sub; //1 -> +, 0 -> -
   wire [0:13]   input_num;
   reg [0:13]   mem_num;

   usr_input user_input(clk_i, rst_i, ps2_clk_i, ps2_data_i, input_num, oper);

   always @(oper[0] or input_num)
      if (oper[0])
        begin
           if (oper == 3'b111) //=
             if (add_sub)
               num = num + mem_num;
             else
               num = num - mem_num;
           else if (oper == 3'b110) //+
             begin
                add_sub = 1;
                mem_num = num;
             end
           else if (oper == 3'b101) //-
             begin
                add_sub = 0;
                mem_num = num;
             end
           else if (oper == 3'b100) //ESC
             num = 0;
        end
      else
        num = input_num;
endmodule
