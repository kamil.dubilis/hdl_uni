`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.03.2024 16:51:14
// Design Name: 
// Module Name: ps2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ps2(
    input wire clk_i,
    input wire rst_i,
    input wire ps2_clk_i,
    input wire ps2_data_i,
    output reg [0:7] char,
    output reg new_char
    );

   wire     ps2_clk_d;
   reg [10:0] scancode;
   reg        trans_rec;
   reg [0:3]  bitN;
   reg        release_sig;

   debounce ps2clkd(clk_i, ps2_clk_i, ps2_clk_d);

   always @(negedge ps2_clk_d or posedge rst_i)
      if (rst_i)
        begin
           bitN = 0;
           scancode = 11'b00000000000;
        end
      else
        begin
           trans_rec = 0;
           scancode[bitN] = ps2_data_i;
           bitN = bitN + 1;
           if (bitN > 10)
             begin
                bitN = 0;
                trans_rec = 1;
             end
        end

   always @(posedge trans_rec or posedge rst_i)
      if (rst_i)
        begin
           release_sig = 0;
           new_char = 0;
        end
      else if (scancode[8:1] == 8'hF0)
        release_sig = 1;
      else if (release_sig)
        begin
           release_sig = 0;
           new_char = 0;
        end
      else
        begin
           char = scancode[8:1];
           new_char = 1;
        end
endmodule
