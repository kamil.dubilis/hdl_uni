`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.03.2024 13:40:21
// Design Name: 
// Module Name: usr_input
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module usr_input(
    input wire clk_i,
    input wire rst_i,
    input wire ps2_clk_i,
    input wire ps2_data_i,
    output reg [0:13] num,
    output reg [0:2] oper
    );

   wire [0:7] char;
   wire new_char;
   wire [0:3] input_digit;

   ps2 ps2input(clk_i, rst_i, ps2_clk_i, ps2_data_i, char, new_char);
   digit digitconv(char, input_digit);

   always @(posedge new_char)
        if (char == 8'h76) //ESC
          begin
             num = 0;
             oper = 3'b100;
          end
        else if (char == 8'h55) //=
          oper = 3'b111;
        else if (char == 8'h79) //+
          oper = 3'b110;
        else if (char ==8'h7b) //-
          oper = 3'b101;
        else
           if(oper[0] == 1)
             begin
                oper = 0;
                num = input_digit;
             end
           else
             num = (num * 10 + input_digit) % 1000;
endmodule
