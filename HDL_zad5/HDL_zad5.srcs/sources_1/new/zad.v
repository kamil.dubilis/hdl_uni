`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 29.02.2024 12:05:19
// Design Name:
// Module Name: zad1
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module zad(
    input clk_i,
    input rst_i,
    input ps2_clk_i,
    input ps2_data_i,
    output [7:0] led7_an_o,
    output [7:0] led7_seg_o
    );

    display disp(clk_i, rst_i, ps2_clk_i, ps2_data_i, led7_an_o, led7_seg_o);
endmodule
